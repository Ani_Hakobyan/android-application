package com.example.user.localizationapp;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private TextView title;
    private TextView text;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title = findViewById(R.id.title);
        title.setText(R.string.title);

        text = findViewById(R.id.text);
        text.setText(R.string.text);
        imageView = findViewById(R.id.image);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0,2,0,"Eng");
        menu.add(0,1,0,"Arm");
        menu.add(0,3,0,"Rus");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 2:
                imageView.setImageResource(R.drawable.img);
                setLocale("en");
                title.setText(getResources().getString(R.string.title));
                text.setText(getResources().getString(R.string.text));
                return true;
            case 3:
                setLocale("kv");
                imageView.setImageResource(R.drawable.image);
                title.setText(getResources().getString(R.string.title));
                text.setText(getResources().getString(R.string.text));
                return true;
            case 1:
                setLocale("hy");
                imageView.setImageResource(R.drawable.aimg);
                title.setText(getResources().getString(R.string.title));
                text.setText(getResources().getString(R.string.text));
                return true;


        }
        return super.onOptionsItemSelected(item);
    }
    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}
